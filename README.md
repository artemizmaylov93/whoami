whoami
Simple HTTP docker service that prints it's container ID

$ docker run -d -p 80:8000 --name whoami -t registry.gitlab.com/artemizmaylov93/whoami

$ curl $(hostname --all-ip-addresses | awk '{print $1}'):8000