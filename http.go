package main

import (
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"runtime"
)

func main() {
	port := os.Getenv("PORT")
	if port == "" {
		port = "8000"
	}

	fmt.Fprintf(os.Stdout, "Listening on :%s\n", port)
	hostname, _ := os.Hostname()
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(os.Stdout, "I'm %s\n", hostname)
		fmt.Fprintf(w, "I'm %s running on %s/%s\n", hostname, runtime.GOOS, runtime.GOARCH)
		// get client ip address
  	ip,_,_ := net.SplitHostPort(r.RemoteAddr)
		// print out the ip address
  	fmt.Fprintf(w, "client ip: %s\n", ip)
		// let's get the request HTTP header "X-Forwarded-For (XFF)"
  	// if the value returned is not null, then this is the real IP address of the user.
  	fmt.Fprintf(w, "client ip X-Forwarded-For: %s\n", r.Header.Get("X-FORWARDED-FOR"));
		ifaces, _ := net.Interfaces()
		for _, i := range ifaces {
			addrs, _ := i.Addrs()
			// handle err
			for _, addr := range addrs {
				var ip net.IP
				switch v := addr.(type) {
				case *net.IPNet:
					ip = v.IP
					fmt.Fprintf(w, "IP local: %s\n", ip)
				case *net.IPAddr:
					ip = v.IP
					fmt.Fprintf(w, "IP container: %s\n", ip)
				}
				//fmt.Fprintln(w, "IP:", ip)
			}
		}
	})
	log.Fatal(http.ListenAndServe(":"+port, nil))
}
